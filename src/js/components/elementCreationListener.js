var observers = [];

var mutationObserver = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
  	if (mutation.type == "childList") {
  		for (var n=0;n<mutation.addedNodes.length;n++) {
	    	for (var i=0;i<observers.length;i++) {
	    		if (mutation.target instanceof HTMLElement) {
	  				var elements = mutation.target.querySelectorAll(observers[i].querySelector);
	  				for (var x=0;x<elements.length;x++) {
	  					observers[i].fireFunction(elements[x], x);
	  				}
	    		}
  			}
  		}
  	}
  });
});

mutationObserver.observe(document.documentElement, {
  attributes: true,
  characterData: true,
  childList: true,
  subtree: true,
  attributeOldValue: true,
  characterDataOldValue: true
});

export default function addObserver(querySelector, fireFunction) {
	observers.push({querySelector: querySelector, fireFunction: fireFunction});
}