import scratchoff from './components/scratchoff';
import addObserver from './components/elementCreationListener';
import toDataUrl from './components/toDataUrl';


const defaults = {
	selector: ".scratch-off-panel",
	dataUrl: false
}

let scratchOffInit = (options) => {

	options = Object.assign({}, defaults, options);

	let styles = document.createElement("style");

	styles.innerHTML = `${options.selector} { position:relative; visibility:hidden; } ${options.selector} canvas { position:absolute;top:0;left:0;width: 100%;height:100%; }`

	document.head.appendChild(styles)

	let overlay;

	var existingDivs = document.querySelectorAll(options.selector);

	for (let i=0;i<existingDivs.length;i++) {
		existingDivs[i].setAttribute("data-init", true);
		if ( overlay = existingDivs[i].getAttribute("data-overlay") || options.dataUrl ) {
			toDataUrl('https://cors-anywhere.herokuapp.com/' + overlay).then((dataUrl) => {
				scratchoff(existingDivs[i], dataUrl);
			});
		} else {
			scratchoff(existingDivs[i]);
		}

	}

	addObserver(`${options.selector}:not([data-init])`, function(el) {
		console.log("testing");
		if ( overlay = el.getAttribute("data-overlay") || options.dataUrl ) {
			toDataUrl('https://cors-anywhere.herokuapp.com/' + overlay).then((dataUrl) => {
				scratchoff(el, dataUrl);
			});
		} else {
			scratchoff(el);
		}
	});
}

if (window) {
	window.scratchOff = scratchOffInit;
}


export default scratchOffInit;