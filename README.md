# Usage

Include the `dist/js/app.bundle.js` into your page, and call the function `scratchOff()` to init on a page

# Target elements

By default it will look and put scratch off panels and all existing and future elements with the selector `.scratch-off-panel`, however by sending options to the inital function, for example;

`scratchOff({ selector: "#idofelement" })`

# Overlay

If you wish to set a different image for the scratch off panel, you can supply one by either adding an image url to the `data-overlay` attribute of the element, or by providing a default image url in the initialisation function, for example;

`scratchOff({dataUrl: "https://ca.slack-edge.com/T0LB8V494-U0QB38TGF-3cd2a852171a-512"})`